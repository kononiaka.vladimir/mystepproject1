$(document).ready(function () {    
    $('.service-menu-item').click(function(){
      $(this)
      .addClass('active')
      .siblings()
      .removeClass('active')
      .closest('.service-menu')
      .find('.service-menu-description')
      .removeClass('active')
      .eq($(this).index())
      .addClass('active')
    })

    $('.work-list-item').hide();
    $('.work-list-item').slice(0,12).show();
    $('.btn-load').click(function(){
        $('.work-list-item:hidden').slice(0,12).show();
        if (!$('.work-list-item').is(':hidden')){
            $(this).hide();
        }
    })

    $('.work-menu-item').click(function() {
        let category = $(this).attr('id');
        if (category == 'all'){
            $('.work-list-item').addClass('hide');
            setTimeout(function(){
                $('.work-list-item').removeClass('hide');
            }, 300);
        } else {
            $('.work-list-item').addClass('hide');
            setTimeout(function(){
                $('.' + category).removeClass('hide');
            }, 300);
        }
    })
    $('.references-text').hide().first().show();
    $('.team-member').hide().first().show();
    $('.team-member-photo').hide().first().show();

    $('.team-slider-photo').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        switchContent();
    })
    $('.slider-arrow.right').click(function(){
        let activeIndex = $('.team-slider-photo.active').index() - 1;
        let newIndex = (activeIndex == 3) ? 0 : activeIndex + 1;
        $(`.team-slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })
    $('.slider-arrow.left').click(function(){
        let activeIndex = $('.team-slider-photo.active').index() - 1;
        let newIndex = (activeIndex == 0) ? 3 : activeIndex - 1;
        $(`.team-slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })
    function switchContent () {
        let activeIndex =  $('.team-slider-photo.active').index() - 1;        
        $(`.references-text:eq(${activeIndex})`).show().siblings('.references-text').hide();
        $(`.team-member:eq(${activeIndex})`).show().siblings('.team-member').hide();
        $(`.team-member-photo:eq(${activeIndex})`).show().siblings('.team-member-photo').hide();
    }
})
